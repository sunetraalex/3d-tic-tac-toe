const WIN_COMBINATIONS = [
    // Horizontals
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    // Verticals
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    // Diagonals
    [0, 4, 8],
    [2, 4, 6],
];

const VIEW = {
    DEFAULT: 'default',
    TOP_DOWN: 'top-down',
};

const TYPE = {
    X: 'X',
    O: 'O',
    Draw: 'Draw',
    Null: '',
};

const gameState = {
    turn: TYPE.X,

    scores: {
        [TYPE.X]: 0,
        [TYPE.O]: 0,
    },

    board: [
        TYPE.Null, TYPE.Null, TYPE.Null,
        TYPE.Null, TYPE.Null, TYPE.Null,
        TYPE.Null, TYPE.Null, TYPE.Null,
    ],

    winner: TYPE.Null,

    view: VIEW.DEFAULT,

    toggleView() {
        this.view = this.view === VIEW.DEFAULT ? VIEW.TOP_DOWN : VIEW.DEFAULT;
    },

    toggleTurn() {
        this.turn = this.turn === TYPE.X ? TYPE.O : TYPE.X;
    },

    getWinnerText() {
        switch (this.winner) {
            case TYPE.X:
            case TYPE.O:
                return `${this.winner} Wins`;
            case TYPE.Draw:
                return this.winner;
            default:
                return '';
        }
    },

    checkWinState() {
        // Board filled with no winners
        const noMoreFreeTiles = this.board.every(cell => cell !== TYPE.Null);
        if (noMoreFreeTiles) {
            this.winner = TYPE.Draw;
        }

        // Check for winning combination
        for (const winCombination of WIN_COMBINATIONS) {
            const hasTurnWon = winCombination.every(index => this.board[index] === this.turn);
            if (hasTurnWon) {
                this.winner = this.turn;
            }
        }

        // Add winner score
        switch (this.winner) {
            case TYPE.Null:
                break;
            case TYPE.Draw:
                this.scores[TYPE.X]++;
                this.scores[TYPE.O]++;
                break;
            default:
                this.scores[this.winner]++;
        }
    },

    resetGame() {
        this.winner = TYPE.Null;
        this.board = this.board.map(() => TYPE.Null);
        this.updateBoardUI();
    },

    setBoardTile(index) {
        // Reset if done
        if (this.winner !== TYPE.Null) {
            this.resetGame();
            return;
        }

        // Assign tile on selected cell
        const currentTile = this.board[index];
        if (currentTile == TYPE.Null) {
            this.board[index] = this.turn;

            this.checkWinState();
            this.toggleTurn();
            this.updateBoardUI();
        }
    },

    updateBoardUI() {
        // Turn
        const turnEl = document.querySelector('.Turn');
        turnEl.dataset.mark = this.turn;

        // Winner
        const winnerBoxEl = document.querySelector('.WinnerBox');
        const winnerEl = document.querySelector('.Winner');
        winnerBoxEl.dataset.mark = this.winner;
        winnerEl.innerText = this.getWinnerText();

        // Scores
        for (const [scoreKey, scoreValue] of Object.entries(this.scores)) {
            const scoreEl = document.querySelector(`.Scores-number[data-type="${scoreKey}"]`);
            scoreEl.innerText = scoreValue;
        }

        // Board
        for (const tileIndex in this.board) {
            const tile = this.board[tileIndex];

            const tileEl = document.querySelector(`.Tile[data-tile="${tileIndex}"]`);
            tileEl.dataset.mark = tile;
        }

        // View
        const cameraEl = document.querySelector('.Camera');
        const viewSwitcherButtonEl = document.querySelector('.ViewSwitcher-button');
        cameraEl.dataset.view = this.view;
        viewSwitcherButtonEl.dataset.view = this.view;
    },
};

const tileClicked = (e) => {
    const tileIndex = Number(e.currentTarget.dataset.tile);

    gameState.setBoardTile(tileIndex);
};

const toggleView = () => {
    gameState.toggleView();
    gameState.updateBoardUI();
};

const panView = (e, pannerVerticalEl, pannerHorizontalEl) => {
    const x = (e.clientX / window.innerWidth).toFixed(2);
    const y = (e.clientY / window.innerHeight).toFixed(2);
    pannerVerticalEl.style.animationDelay = `-${y}s`;
    pannerHorizontalEl.style.animationDelay = `-${x}s`;
};

const mousemoved = (pannerVerticalEl, pannerHorizontalEl) =>  (e) => {
    panView(e, pannerVerticalEl, pannerHorizontalEl);
};

const init = () => {
    const tiles = document.querySelectorAll('.Tile');
    for (const tileEl of tiles) {
        tileEl.addEventListener('click', tileClicked);
    }

    const viewSwitcherEl = document.querySelector('.ViewSwitcher');
    viewSwitcherEl.addEventListener('click', () => {
        toggleView();
    });

    /*
    const pannerVerticalEl = document.querySelector('.Panner--vertical');
    const pannerHorizontalEl = document.querySelector('.Panner--horizontal');
    const mousemoveCallback = mousemoved(pannerVerticalEl, pannerHorizontalEl);

    // Stop tilt with mouse
    document.addEventListener('mousemove', mousemoveCallback, true);
    // Stop mouve movement on mobile
    document.addEventListener('touchstart', (e) => {
        console.log('START', e);
        document.removeEventListener('mousemove', mousemoveCallback);
    }, { once: true });
    document.addEventListener('touchmove', () => {
        document.removeEventListener('mousemove', mousemoveCallback);
    }, { once: true });
    */

    gameState.updateBoardUI();
};

document.addEventListener('DOMContentLoaded', init);
