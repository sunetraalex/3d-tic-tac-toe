class BaseElement extends HTMLElement {
    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
    }

    connectedCallback() {
        this.shadowRoot.innerHTML = `
            <style>
                ${this.css}
            </style>

            ${this.template}
        `;
    }

    get css() {
        return `
            :host {
                display: block;
                position: absolute;
                top: 0;
                left: 0;
            }
        `;
    }

    get template() {
        return `
            <slot></slot>
        `;
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
        console.log(attrName, oldValue, newValue);
        if (oldValue !== newValue) {
            this.style.setProperty(`--${attrName}`, newValue);
        }
    }
}

class Pos extends BaseElement {
    static get observedAttributes() {
        return ['x', 'y', 'z', 'rx', 'ry', 'rz', 'scale'];
    }

    get css() {
        return `
            :host {
                --x: 0em;
                --y: 0em;
                --z: 0em;
                --rx: 0deg;
                --ry: 0deg;
                --rz: 0deg;
                --scale: 1;

                display: block;
                position: absolute;
                top: 0;
                left: 0;
                transform:
                    translateX(var(--x))
                    translateY(var(--y))
                    translateZ(var(--z))
                    rotateX(var(--rx))
                    rotateY(var(--ry))
                    rotateZ(var(--rz))
                    scale(var(--scale));
                transform-style: preserve-3d;
            }

            :host([transition]) {
                transition: transform .5s;
            }
        `;
    }
}

customElements.define('css-pos', Pos);

class Cube extends Pos {
    static get observedAttributes() {
        return [...super.observedAttributes, 'width', 'height', 'depth', 'texture'];
    }

    get css() {
        return `
            ${super.css}
            :host {
                --width: 50em;
                --height: 50em;
                --depth: 50em;
                --texture: var(--Texture-wood);
            }

            .Cube {
                position: absolute;
                top: 0;
                left: 0;
                transform-style: preserve-3d;
            }

            .Cube-face {
                position: absolute;
                top: 0;
                left: 0;
                transform-style: preserve-3d;
                width: var(--Cube-face-width);
                height: var(--Cube-face-height);
                background: var(--texture);
                background-size: cover;
                backface-visibility: hidden;
                box-shadow: inset 0 0 5em rgba(0, 0, 0, .3);
                transform-origin: left top;
            }

            /* Front */
            .Cube-face:nth-child(1) {
                --Cube-face-width: var(--width);
                --Cube-face-height: var(--depth);
                transform: rotateX(-90deg) translateY(calc(var(--depth) * -1)) translateZ(var(--height));
            }

            /* Back */
            .Cube-face:nth-child(2) {
                --Cube-face-width: var(--width);
                --Cube-face-height: var(--depth);
                transform: rotateX(90deg);
            }

            /* Left */
            .Cube-face:nth-child(3) {
                --Cube-face-width: var(--height);
                --Cube-face-height: var(--depth);
                transform: rotateX(90deg) rotateY(270deg) translateX(calc(var(--height) * -1));
            }

            /* Right */
            .Cube-face:nth-child(4) {
                --Cube-face-width: var(--height);
                --Cube-face-height: var(--depth);
                transform: rotateX(90deg) rotateY(90deg) translateZ(var(--width));
            }

            /* Top */
            .Cube-face:nth-child(5) {
                --Cube-face-width: var(--width);
                --Cube-face-height: var(--height);
                transform: translateZ(var(--depth));
            }

            /* Bottom */
            .Cube-face:nth-child(6) {
                --Cube-face-width: var(--width);
                --Cube-face-height: var(--height);
                transform: rotateX(180deg) translateY(-100%);
            }

            :host([transition]) {
                transition: transform .5s;
            }

            :host([drop-shadow]) .Cube-face:nth-child(6) {
                transform: translate3d(0, 0, 0);
                box-shadow: 0 0 10em rgba(0, 0, 0, .6);
            }
        `;
    }

    get template() {
        return `
            <div class="Cube">
                <div class="Cube-face">
                    <slot name="front"></slot>
                </div>
                <div class="Cube-face">
                    <slot name="back"></slot>
                </div>
                <div class="Cube-face">
                    <slot name="left"></slot>
                </div>
                <div class="Cube-face">
                    <slot name="right"></slot>
                </div>
                <div class="Cube-face">
                    <slot name="top"></slot>
                </div>
                <div class="Cube-face">
                    <slot name="bottom"></slot>
                </div>
            </div>
            <slot></slot>
        `;
    }
}

customElements.define('css-cube', Cube);
